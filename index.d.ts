export interface NeuralLayer {
    type: string;
    name?: string;
    next: string;
}

export interface LayerTypeDefinition {
    process(vector: Vector, layer: NeuralLayer): Vector;
    build(...buildArgs: any): NeuralLayer;
    mutate(layer: NeuralLayer, amount: number): void;
    breed(layer1: NeuralLayer, layer2: NeuralLayer): NeuralLayer;
    applyStep(layer1: NeuralLayer, layer2: NeuralLayer, fitness: number): NeuralLayer;
}

export interface Vector {
    data: number[];
    dims: number;

    co(coord: number): number;
    size(): number;
    sum(): number;
    add(other: Vector): Vector;
    pow(power: number): Vector;
    sub(other: Vector): Vector;
    multiplyVec(other: Vector): Vector;
    divideVec(other: Vector): Vector;
    multiplyFac(factor: number): Vector;
    divideFac(denominator: number): Vector;
    combiner(combiner: (a: number, b: number) => number): (other: Vector) => Vector;
    map(mapper: (value: number) => number): Vector;
    dot(other: Vector): number;
}

export interface VectorLike {
    data: number[];
    dims: number;
}

export interface NeuralNetworkData {
    layers: {[layerName: string]: NeuralLayer};
    first: string;
}

export interface NetworkStaticEvolveOptions {
    population?: number;
    maxGens?: number;
    maxMutation?: number;
    mutationDecay?: number;
    stepFactor?: number;
    quota?: number;
    random?: boolean;
    debug?: boolean;

    inputSet: Vector[];
    expectedSet: Vector[];
}

export interface NetworkBasicEvolveOptions {
    population?: number;
    maxGens?: number;
    maxMutation?: number;
    mutationDecay?: number;
    stepFactor?: number;
    quota?: number;
    random?: boolean;
    debug?: boolean;
}

export interface NetworkDynamicEvolveOptions {
    population?: number;
    maxGens?: number;
    maxMutation?: number;
    mutationDecay?: number;
    stepFactor?: number;
    quota?: number;
    random?: boolean;
    debug?: boolean;

    step(currentStep: NeuralNetwork, generation?: number): number;
    postStep?(bestSoFar: NeuralNetwork): void;
}

export interface NetworkDynamicBatchEvolveOptions {
    population?: number;
    maxGens?: number;
    maxMutation?: number;
    mutationDecay?: number;
    stepFactor?: number;
    quota?: number;
    random?: boolean;
    debug?: boolean;
    batchPop: number;

    stepAll(steps: NeuralNetwork[], generation: number | null, batchIndex: number): void;
    postStep?(bestSoFar: NeuralNetwork): void;
}

export interface SequentialLayerData {
    size?: number;
    type?: string;

    pre?: string;
    post?: string;
}

export interface SequentialCompositeLayerData {
    size?: number;
    type?: 'sequence' | 'combo';

    pre?: string;
    post?: string;

    parts: (SequentialLayerData | SequentialCompositeLayerData)[];
}

export interface NeuralNetwork {
    data: NeuralNetworkData;
    id: string;

    json(): string;
    compute(input: number[] | Vector): Vector;
    computeAsync(input: number[] | Vector): Promise<Vector>;
    clone(): NeuralNetwork;
    mutate(amount?: number): NeuralNetwork;
    applyStep(other: NeuralNetwork, fitness: number): NeuralNetwork;
    
    error(inputSet: (number | Vector)[], expectedSet: (number | Vector)[]): number;
    staticFitness(inputSet: (number | Vector)[], expectedSet: (number | Vector)[]): number;
    evolve(options: NetworkEvolveOptions): Promise<NeuralNetwork>;
}

export interface VectorifierStringArgument {
    type: 'simplestring' | 'string';
    size: number;
    default?: string;
}

export interface VectorifierNumberArgument {
    type: 'number';
    min: number;
    max: number;
    rounded?: boolean;
}

export interface VectorifierNumberArrayArgument {
    type: 'numbers';
    size: number;
    min: number;
    max: number;
    rounded?: boolean;
}

export interface Vectorifier {
    getSize(): number;
    encode(data: {string: number | string}): number[];
    decode(encoded: number[]): {string: number | string};
}

export interface GANOptions {
    size?: {
        noise?: number;
        output: number;
    };

    generatorLayers?: (SequentialLayerData | SequentialCompositeLayerData)[];
    discriminatorLayers?: (SequentialLayerData | SequentialCompositeLayerData)[];
    outputType?: string;
}

export interface GANExtraEvolveOptions {
    discriminatorTrainOptions?: NetworkEvolveOptions;
}

export type NetworkEvolveOptions = NetworkStaticEvolveOptions | NetworkDynamicEvolveOptions | NetworkDynamicBatchEvolveOptions;
export type VectorifierArgument = ( VectorifierStringArgument | VectorifierNumberArgument | VectorifierNumberArrayArgument );
export type VectorSet = (Vector | number[])[];

//

export function greataptic(data: NeuralNetworkData): NeuralNetwork

export function $vec(vector: number[] | VectorLike): Vector;
export namespace $vec {
    export function is(something: any): boolean;

    export interface VectorMapFillOptions {
        length: number,
        map: (index: number, vectorArray: number[]) => number
    }

    export interface VectorFillOptions {
        length: number,
        value?: number
    }
    
    export function fill(opts: VectorMapFillOptions | VectorFillOptions): Vector;

    export function random(dims: number): Vector;
    export function randomOne(): number;
    export function randomSize(dims: number, size: number): Vector;
    export function zero(dims: number): Vector;
}

export var layerTypes: {[typename: string]: LayerTypeDefinition}

export function activate(kind: string, value: number): number;
export function isNetwork(something: any): boolean;
export function breed(networks: NeuralNetwork[]): NeuralNetwork;
export function sequential(inputSize: number, layers: (SequentialLayerData | SequentialCompositeLayerData)[]): NeuralNetwork;
export function fromJSON(json: string): NeuralNetwork;
export function createVectorifier(args: VectorifierArgument[]): Vectorifier;
    
export class GAN {
    size: {
        noise: number;
        output: number;
    };

    generator: NeuralNetwork;
    discriminator: NeuralNetwork;

    constructor(properties: GANOptions);
    
    rate(generated: (Vector | number[])): number;
    evolve(realData: VectorSet, options: NetworkEvolveOptions & GANExtraEvolveOptions): Promise<void>;
    makeNoise(): Vector;
    generate(optionalGeneratorOverride?: NeuralNetwork): Vector;
}

export class StaticMultiEvolver {
    public nets: Map<string, NeuralNetwork>;
    public processor: ({[name]: NeuralNetwork}, input: any) => Vector;

    constructor(nets: Map<string, NeuralNetwork> | {[name: string]: NeuralNetwork}, processor: ({[name]: NeuralNetwork}, input: any) => Promise<Vector> | Vector);

    cloneNets(): Map<string, NeuralNetwork>;
    compute(input: any, netMap?: Map<string, NeuralNetwork>): Promise<Vector>;
    error(nets: Map<string, NeuralNetwork>, inputSet: any[], expectedSet: VectorSet): Promise<number>;
    staticFitness(nets: Map<string, NeuralNetwork>, inputSet: any[], expectedSet: VectorSet): Promise<number>;
    getFitness(nets: Map<string, NeuralNetwork>, inputSet: any[], expectedSet: VectorSet): Promise<number>;
    evolveStatic(inputSet: any[], expectedSet: VectorSet, options: NetworkBasicEvolveOptions): Promise<Map<string, NeuralNetwork>>;
}